<!DOCTYPE html>
<html lang="en-US">
<head>
    <!--Contains page title, scripts and links to css !-->
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/head.php';?>

</head>

<body>
<!--This holds the Navigation Bar on the top !-->
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/CARRIER_TOP_BLUE_NAVIGATION-BAR.php';?>
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_dropdown_menus.php';?>


<!--Pagination !-->
            <div class="center1">
                <div class="page-numbers1">
                    <a href="24vna6-25vna4-service-manual-page2.php">First </a>
                    <a href="24vna6-25vna4-service-manual-page2.php">&lt </a>
                    <a href="24vna6-25vna4-service-manual-page3.php" style="background-color:#7aa72d;border-radius: 10px;">3</a>
                    <a href="24vna6-25vna4-service-manual-page4.php" >4</a>
                    <a href="24vna6-25vna4-service-manual-page4.php" >&gt; </a>
                    <a href="24vna6-25vna4-service-manual-page40.php" >Last </a>
                </div></div>

            <!--<form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onkeyup="showResult(this.value)">
                            <div id="livesearch"></div>
                            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
                        </form> !-->
        </div>

</div>
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/GREY_PRODUCT_BANNER.php';?>

<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_service_manual_bannertext_and_feedbacktab.php';?>

</div></div>


<div class="container">  
  <div class="row">
    <div class="col-sm-6" >

	<h4>Electrical</h4>
		<table>
      	<tr><th style="background-color:orange"><span style="font-size:20px;">&#9888;</span>WARNING &dash;&dash; ELECTRICAL SHOCK HAZARD!</th></tr>
        <tr><td>Failure to follow this warning could result in personal injury or death.
		Exercise extreme caution when working on any electrical components. Shut off all power to system prior to troubleshooting. Some troubleshooting techniques require power to remain on. In these instances, exercise extreme caution to avoid danger of electrical shock. ONLY TRAINED SERVICE PERSONNEL SHOULD PERFORM ELECTRICAL TROUBLESHOOTING.

		</td></tr>
        </table><br>
<br>
        <p><b>Aluminum Wire</b></p>
		<table>
      	<tr><th style="background-color:yellow"><span style="font-size:20px;">&#9888;</span>CAUTION &dash;&dash; UNIT OPERATION AND SAFETY HAZARD!</th></tr>
        <tr><td>Failure to follow this caution may result in equipment damage or improper operation.<br><br>
		Aluminum wire may be used in the branch circuit (such as the circuit between the main and unit disconnect), but only copper wire may be used between the unit disconnect and the unit. 

		</td></tr>
        </table>
		<br>
		<p>Whenever aluminum wire is used in branch circuit wiring with this unit, adhere to the following recommendations.  <br><br>
		Connections must be made in accordance with the National Electrical Code (NEC), using connectors approved for aluminum wire. The connectors must be UL approved (marked Al/Cu with the UL symbol) for the application and wire size. The wire size selected must have a current capacity not less than that of the copper wire specified, and must not create a voltage drop between service panel and unit in excess of 2 of unit rated voltage. To prepare wire before installing connector, all aluminum wire must be "brush-scratched" and coated with a corrosion inhibitor such as Pentrox A. When it is suspected that connection will be exposed to moisture, it is very important to cover entire connection completely to prevent an electrochemical action that will cause connection to fail very quickly. Do not reduce effective size of wire, such as cutting off strands so that wire will fit a connector. Proper size connectors should be used. Check all factory and field electrical connections for tightness. This should also be done after unit has reached operating temperatures, especially if aluminum conductors are used.</p>
		
		<h4>Electrical System Overview</h4>
		<p>The electrical system consists of four main primary components and sub-systems as listed below and shown in <a href="#figure1">Fig. 1</a></p>
		<ul>
			<li>Terminal Block</li>
			<li>Transformer</li>
			<li>Compressor and Fan Sub-System</li>
			<li>Control Sub-System</li>
		
		</ul>
		</div>
	
	
  

    <div class="col-sm-6" >
		
		<img src="images/A200232.svg" width="100%" alt="" id="figure1" ><br>
		<p style="text-align:center"><b>Fig. 1 – Electrical System Overview</b></p><br>
		
		<p><b>Terminal Block</b>
		<br>The terminal block provides the connection point for high voltage field power wiring</p><br>
		
		<p><b>Transformer</b></p>
		<table>
      	<tr><th style="background-color:yellow"><span style="font-size:20px;">&#9888;</span>CAUTION &dash;&dash; Equipment Damage Hazard!</th></tr>
        <tr><td>Failure to follow this caution may result in equipment damage or improper operation.<br><br>
		Do not connect the equipment 24VAC supply (Rc) to the indoor
		equipment 24VAC supply (Rh).

		</td></tr>
        </table><br>
		
		<p><b>IMPORTANT:</b> The 24VAC power (Rc) in the PCM comes from the
		transformer in the equipment. The phase of this transformer connection
		is not controlled relative to the phase of the power provided by the
		transformer in the indoor equipment (Rh). Wiring requirements in this
		manual do not connect Rc and Rh together. For any non-standard
		wiring, care should be taken to make sure that Rc and Rh are not
		connected together. Doing so may result in destroying one or both of the
		transformers in this and the indoor equipment.<br><br>
		The transformer converts line voltage power to low voltage power
		required by the control system. The transformer also provides isolation
		between the line voltage and low voltage systems.</p>
<br>
		<p><b>Compressor / Fan Sub-System</b>
		<br>The compressor / fan subsystem consists of the following components:</p>
		
		<ul>
			<li>Variable speed compressor</li>
			<li>Variable speed fan</li>
			<li>Variable Frequency Drive (VFD) sub-system for driving the compressor and fan</li>
		
		</ul>
		
		The VFD sub-system consists of several components that are shown
		conceptually in <a href="24vna6-25vna4-service-manual-page4.php#figure2">Fig. 2</a> with detailed wiring shown in <a href="24vna6-25vna4-service-manual-page4.php#figure3">Fig. 3</a>. The
		function of each component is described in further detail in the following
		section.</p>
		
		
	  </div>
		


  </div>
</div>



<div class=container style="padding-bottom:55px">
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/COPYRIGHT.php';?>
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_footertext.php';?>
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/FOOTER.php';?>

<p style="text-align:center; font-size: 11px"><br><b> Page 3</b></p>

</div>



</body>


<div class="footer2">

    <div class="center">
        <div class="page-numbers">
            <a href="24vna6-25vna4-service-manual-page2.php">First </a>
            <a href="24vna6-25vna4-service-manual-page2.php">&lt </a>
            <a href="24vna6-25vna4-service-manual-page3.php" style="background-color:#7aa72d;border-radius: 10px;">3</a>
            <a href="24vna6-25vna4-service-manual-page4.php" >4</a>
            <a href="24vna6-25vna4-service-manual-page4.php" >&gt; </a>
            <a href="24vna6-25vna4-service-manual-page40.php" >Last </a>
        </div>
    </div>
    <div>
</html>

