<!DOCTYPE html>
<html lang="en-US">
<head>
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/title-Carrier-Installation-Manual.php';?>

    <!--Links and scripts, including tooltips and livesearch!-->
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/Heading.php';?>
</head>
<body>
<!--This holds the Navigation Bar on the top !-->

<!-- Navigation Bar !-->
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/Carrier-with-logo-navbar-blank.php';?>

<!-- PDF indices are condensed into dropdown menus !-->
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_dropdown_menus.php';?>

<!--GOTO pagination !-->
<div class="center">
    <div class="dropdown">
        <button class="btn btn-outline-secondary dropdown-toggle " type="button" data-toggle="dropdown">Go to
        </button>
        <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/GOTO-PAGINATION-Carrier-24vna6-SM.php';?>

        <!--Pagination !-->
        <div class="center1">
            <div class="page-numbers1" >
                    <a href="" style="color:grey;border-radius: 10px;">&lt </a>
                    <a href="24vna6-25vna4-service-manual-page2.php" style="background-color:#7aa72d;border-radius: 10px;">2</a>
                    <a href="24vna6-25vna4-service-manual-page3.php" >3</a>
                    <a href="24vna6-25vna4-service-manual-page3.php" >&gt; </a>

            </div></div>

    </div>
    </nav>
</div>
<!--blank grey banner !-->
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/GREY_PRODUCT_BANNER.php';?>
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_service_manual_bannertext_and_feedbacktab.php';?>
</div></div>



<div class="container">

    <h3>Unit Identification</h3>
    <p>The unit is identified using a 16 digit model number structure.  It is recommended providing the complete 16 digit model number when ordering replacement parts to insure receiving the correct parts.</p>
    <br>

    <p style="text-align:center"><b>Model Number Nomenclature</b></p>



    <table  style="width:100%; border: none">

        <tr class="noborder">
            <td>1</td>
            <td>2</td>
            <td>3</td>
            <td>4</td>
            <td>5</td>
            <td>6</td>
            <td>7</td>
            <td>8</td>
            <td>9</td>
            <td>10</td>
            <td>11</td>
            <td>12</td>
            <td>13</td>
            <td>14</td>
            <td>15</td>
            <td>16</td>

        </tr>

        <tr class="noformat">
            <td>2</td>
            <td>4</td>
            <td>V</td>
            <td>N</td>
            <td>A</td>
            <td>6</td>
            <td>3</td>
            <td>6</td>
            <td>A</td>
            <td>0</td>
            <td>0</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>
            <td>0</td>

        </tr>
        <tr class="noformat">
            <td colspan="2">Product Series</td>
            <td>Product Family</td>
            <td>Tier</td>
            <td>Major Series</td>
            <td>SEER</td>
            <td rowspan="2" colspan="2">Cooling Capacity 1,000 Btuh (nominal)</td>
            <td>Variations</td>
            <td>Open</td>
            <td>Open</td>
            <td>Voltage</td>
            <td>Minor Series</td>
            <td>Open</td>
            <td>Compressor Identification</td>
            <td>Open</td>
        </tr>

        <tr class="noformat">
            <td colspan="2">24=AC<br>
                25=HP</td>

            <td>V=Variable Speed</td>
            <td>Infinity Series</td>
            <td>A=Puron</td>
            <td>6=Up to 26 SEER<br>
                4=Up to 24 SEER</td>
            <td>A = Standard</td>
            <td>0=Not Defined</td>
            <td>0=Not Defined</td>
            <td> 3=208/230-1</td>
            <td></td>
            <td>0=Not Defined</td>
            <td>4=VS Scroll <br> 5=VS Rotary</td>
            <td>0=Not Defined</td>
        </tr>
    </table>

    <br>


    <p style="text-align:center"><b>SERIAL NUMBER NOMENCLATURE</b></p>

    <table style="width:100%;border:none;">
        <tr class="noborder" >
            <td colspan="3" style="border-bottom: 1px solid black">01</td>
            <td colspan="2">18</td>
            <td colspan="2" style="border-bottom: 1px solid black">E</td>
            <td style="width:10%"></td>
            <td colspan="2" style="border-bottom: 1px solid black">00001</td>
            <td style="width:16%"></td>
        </tr>

        <tr class="noborder">
            <td colspan="3"></td>
            <td colspan="2"></td>
            <td colspan="2"></td>
            <td></td>
            <td colspan="2"></td>
            <td></td>
        </tr>

        <tr class="noborder">
            <td height="65" colspan="2" style="border-bottom: 1px solid black; border-right: 1px solid black">Week of <br>Manufacture</td>
            <td style="width:15%; border-bottom: 0"></td>
            <td style="width:9%; border-right: 1px solid black"></td>
            <td style="width:8%"></td>
            <td style="width:6%"></td>
            <td style="width:6%; border-left:1px solid black;"></td>
            <td></td>
            <td style="width:8%"></td>
            <td colspan="2" style="border-bottom: 1px solid black; border-left: 1px solid black">Serial Number</td>
        </tr>

        <tr class="noborder">
            <td colspan="2"></td>
            <td colspan="3"></td>

            <td colspan="2"></td>
            <td></td>
            <td colspan="2"></td>
            <td></td>
        </tr>

        <tr class="noborder">
            <td style="width:9%"></td>
            <td style="width:6%"></td>
            <td colspan="2" style="border-bottom: 1px solid black; border-right: 1px solid black">Year of Manufacture</td>
            <td></td>
            <td></td>
            <td colspan="4" style="border-bottom: 1px solid black; border-left: 1px solid black">Manufacturing Site<br> E = Collierville, TN<br>X = Monterrey, Mexico</td>
            <td></td>
        </tr>

        <tr class="noborder">
            <td colspan="3"></td>
            <td colspan="2"></td>
            <td colspan="2"></td>
            <td></td>
            <td colspan="2"></td>
            <td></td>
        </tr>

    </table>
    <br><br>







	<div class="row">
		<div class="col-sm-6" >
			<h4>Safety Considerations</h4>
			<p>Installation, service, and repair of these units should be attempted only by trained service technicians familiar with standard service instruction and training material.<br><br>
				All equipment should be installed in accordance with accepted practices and unit Installation Instructions, and in compliance with all national and local codes. Power should be turned off when servicing or repairing electrical components. Extreme caution should be observed when troubleshooting electrical components with power on.
				Observe all warning notices posted on equipment and in instructions or manuals.</p>

			<table>
				<tr><th style="background-color:orange"><span style="font-size:20px;">&#9888;</span>WARNING &dash;&dash; ELECTRICAL SHOCK HAZARD!</th></tr>
				<tr><td>Failure to follow this warning could result in personal injury or death.
					Before installing, modifying, or servicing system, main electrical disconnect switch must be in the OFF position. There may be more than 1 disconnect switch. Lock out and tag switch with a suitable warning label.

				</td></tr>
			</table><br>

			<table>
				<tr><th style="background-color:orange"><span style="font-size:20px;">&#9888;</span>WARNING &dash;&dash; ELECTRICAL HAZARD &dash; HIGH VOLTAGE!</th></tr>
				<tr><td>Failure to follow this warning could result in personal injury or death.
					Electrical components may hold charge.  DO NOT remove control box cover for 2 minutes after power has been removed from unit.<br><br>
					PRIOR TO TOUCHING ELECTRICAL COMPONENTS:
					Verify less than zero (0) vdc voltage at VFD connections shown on cover.
				</td></tr>
			</table><br>

			<table>
				<tr><th style="background-color:yellow"><span style="font-size:20px;">&#9888;</span>CAUTION &dash;&dash; CUT HAZARD!</th></tr>
				<tr><td>Failure to follow this caution may result in personal injury.
					Sheet metal parts may have sharp edges or burrs. Use care and wear appropriate protective clothing and gloves when handling parts.

				</td></tr>
			</table>


		</div>




		<div class="col-sm-6" >
			<table>
				<tr><th style="background-color:orange"><span style="font-size:20px;">&#9888;</span>WARNING &dash;&dash; UNIT OPERATION AND SAFETY HAZARD!</th></tr>
				<tr><td>Failure to follow this warning could result in personal injury or equipment damage.<br><br>
					Puron&reg; (R-410A) systems operate at higher pressures than standard R-22 systems. Do not use R-22 service equipment or components on Puron equipment. Ensure service equipment is rated for Puron®.
				</td></tr>
			</table><br>

			<p>Refrigeration systems contain refrigerant under pressure. Extreme caution should be observed when handling refrigerants. Wear safety glasses and gloves to prevent personal injury. During normal system operations, some components are hot and can cause burns. Rotating fan blades can cause personal injury. Appropriate safety considerations are posted throughout this manual where potentially dangerous techniques are addressed.<br><br>
				If you do not understand any of the warnings, contact your product distributor for better interpretation of the warnings. </p>

			<h4>General information</h4>
			<p>The Greenspeed Intelligence heat pump and air conditioner features the latest variable speed technology. The heart of the system is the Samsung high side rotary or scroll variable speed compressor powered through the use of the Samsung variable frequency drive (VFD) control. By combining the Primary Control Module (PCM), an ECM outdoor fan, Samsung VFD, Samsung variable speed compressor and the Infinity Series outdoor cabinet, the HP unit achieves a Seasonal Energy Efficiency Ratio (SEER) of up to 24 and up to 13 Heating Seasonal Performance Factor (HSPF) and the AC achieves up to 26 SEER.<br><br>
				To ensure all of the above technology provides the ultimate in comfort, it is combined with either a fan coil or Variable Speed Gas furnace controlled with a two wire communication Infinity wall control.</p>


		</div>
	</div>
</div>


</body>
<div class=container style="padding-bottom:55px">
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/COPYRIGHT.php';?>
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_footertext.php';?>
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/FOOTER.php';?>

    <p style="text-align:center; font-size: 11px"><br><b> Page 2</b></p>
</div>
</div>

<div class="footer2">
    <div class="center2">
        <div class="dropdown">
            <button class="btn btn-outline-secondary " type="button" data-toggle="dropdown">Go to
                <span class="fa fa-caret-up"></span></button>
            <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/GOTO-PAGINATION-Carrier-24vna6-SM.php';?>
            <div class="page-numbers">
            <a href="" style="color:grey;border-radius: 10px;">&lt </a>
            <a href="24vna6-25vna4-service-manual-page2.php" style="background-color:#7aa72d;border-radius: 10px;">2</a>
            <a href="24vna6-25vna4-service-manual-page3.php" >3</a>
            <a href="24vna6-25vna4-service-manual-page3.php" >&gt; </a>
        </div>
    </div>
    </div>
</div>
</div>

</html>

