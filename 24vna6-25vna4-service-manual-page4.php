<!DOCTYPE html>
<html lang="en-US">
<head>
    <!--Contains page title, scripts and links to css !-->
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/head.php';?>

</head>

<body>
<!--This holds the Navigation Bar on the top !-->
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/CARRIER_TOP_BLUE_NAVIGATION-BAR.php';?>
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_dropdown_menus.php';?>



<!--Pagination !-->
            <div class="center1">
                <div class="page-numbers1">
                    <a href="24vna6-25vna4-service-manual-page2.php">First </a>
                    <a href="24vna6-25vna4-service-manual-page3.php">&lt </a>
                    <a href="24vna6-25vna4-service-manual-page4.php" style="background-color:#7aa72d;border-radius: 10px;">4</a>
                    <a href="24vna6-25vna4-service-manual-page5.php" >5</a>
                    <a href="24vna6-25vna4-service-manual-page5.php" >&gt; </a>
                    <a href="24vna6-25vna4-service-manual-page40.php" >Last </a>
                </div></div>

            <!--<form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" onkeyup="showResult(this.value)">
                            <div id="livesearch"></div>
                            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
                        </form> !-->
        </div>

</div>
<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/GREY_PRODUCT_BANNER.php';?>

<?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_service_manual_bannertext_and_feedbacktab.php';?>

</div></div>

<div class="container">  
	<img src="images/A200234.svg" height="400px" alt="" id="figure2" >
	<p style="text-align:center"><b>Fig. 2 – Compressor / Fan Sub-System Diagram</b></p>
	<br>

	
	<img src="images/346116-101.svg" height="800px" alt="" id="figure3">
	<p style="text-align:center"><b>Fig. 3 – Wiring Diagram — 24VNA6 / 25VNA4 Model sizes 2 - 5 tons, 208/230-1</b></p>
	<br>
</div>
  <div class="row">
    <div class="col-sm-6">
		</div>
	
	
  

    <div class="col-sm-6">
	  </div>
  </div>




<div class=container style="padding-bottom:55px">
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/COPYRIGHT.php';?>
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/carrier_24VNA6_25VNA4_footertext.php';?>
    <?php include '/var/www/html/docs.mlctraining.com/techdocs/PHP/FOOTER.php';?>

<p style="text-align:center; font-size: 11px"><br><b> Page 4</b></p>

</div>



</body>


<div class="footer2">

    <div class="center">
        <div class="page-numbers">
             <a href="24vna6-25vna4-service-manual-page2.php">First </a>
                    <a href="24vna6-25vna4-service-manual-page3.php">&lt </a>
                    <a href="24vna6-25vna4-service-manual-page4.php" style="background-color:#7aa72d;border-radius: 10px;">4</a>
                    <a href="24vna6-25vna4-service-manual-page5.php">5</a>
                    <a href="24vna6-25vna4-service-manual-page5.php" >&gt; </a>
                    <a href="24vna6-25vna4-service-manual-page40.php" >Last </a>
        </div>
    </div>
    <div>
</html>

